import { RequestMock, Selector } from 'testcafe'; // first import testcafe selectors

// Arrange
const horse1Name = "Thunderdash11";
const horse2Name = "Thunderdash22";
const horse1Id = "6e6c9cc4-1092-4cd4-8cb0-e4ab46446d67";
const horse2Id = "1e6c9cc4-1093-4cd4-8cb0-a4ab46446127"
const addButton = Selector('span.MuiButton-label').withText('ADD');
const nameInput = Selector('#name-input');
const foodInput = Selector('#favouriteFood-input');
const saveButton = Selector('span.MuiButton-label').withText('SAVE');

var mock = RequestMock()
    .onRequestTo('http://localhost:3016/horse')
    .respond([{
        id: horse1Id,
        name: horse1Name,
        profile: { favouriteFood: "Hot Chips", physical: { height: 200, weight: 450 } }
    }, {
        id: horse2Id,
        name: horse2Name,
        profile: { favouriteFood: "testHot Chips", physical: { height: 300, weight: 350 } }
    }], 200, { 'Access-Control-Allow-Origin': '*' })
    .onRequestTo('http://localhost:3016/horse').respond(
        "1e6c9c34-1092-4cd4-8cb0-e4ab46441234",
        200, { 'Access-Control-Allow-Origin': '*' });

fixture`Horses test`.page // declare the fixture
    `http://localhost:3000/`
    .requestHooks(mock); // specify the start page

// Test
test('Should able to add new horse', async t => {
    const newHorseName = 'new horse'
    const newHorseExist = Selector('td.MuiTableCell-root').withText(newHorseName).exists;

    await t.click(addButton)
        .typeText(nameInput, newHorseName)
        .typeText(foodInput, 'test food')
        .click(saveButton);

    await t.expect(newHorseExist).ok();
});

test('Should not allow user to add new horse if no name provided', async t => {
    await t.click(addButton)
        .typeText(foodInput, 'test food')
        .click(saveButton);

    await t.expect(nameInput.withAttribute('aria-invalid', 'true').exists).ok();
});

test('Should able to update existing horse', async t => {
    const newHorseName = 'new horse'
    const newHorseExist = Selector('td.MuiTableCell-root').withText(newHorseName).exists;
    const oldHorseExist = Selector('td.MuiTableCell-root').withText(horse1Name).exists;

    const editButton = Selector('button.MuiButtonBase-root ').withAttribute('aria-label', `edit ${horse1Name}`);

    await t.click(editButton)
        .click(nameInput)
        .pressKey('ctrl+a delete')
        .typeText(nameInput, newHorseName)
        .click(saveButton);

    await t.expect(newHorseExist).ok();
    await t.expect(oldHorseExist).notOk();
});

test('Should able to view horse', async t => {
    const viewButton = Selector(`#view-${horse1Id}`);
    const dialog = Selector('div.MuiDialog-paper').withAttribute('role', 'dialog');
    const firstHorseExists = Selector('div').withAttribute("data-testid", "compare-name").withText(horse1Name).exists;
    await t.click(viewButton);

    await t.expect(dialog.exists).ok()
        .expect(firstHorseExists).ok();
});

test('Should able compare two horse', async t => {
    const horse1Checkbox = Selector(`#check-${horse1Id}`);
    const horse2Checkbox = Selector(`#check-${horse2Id}`);
    const compareButton = Selector('button.MuiButtonBase-root').withText('COMPARE');
    const compareDialog = Selector('div.MuiDialog-paper').withAttribute('role', 'dialog');
    const firstHorseExists = Selector('div').withAttribute("data-testid", "compare-name").withText(horse1Name).exists;
    const secondHorseExists = Selector('div').withAttribute("data-testid", "compare-name").withText(horse2Name).exists;

    await t.click(horse1Checkbox)
        .click(horse2Checkbox)
        .click(compareButton);

    await t.expect(compareDialog.exists).ok()
        .expect(firstHorseExists).ok()
        .expect(secondHorseExists).ok();
});

// TODO move to a separate page
var multipleDataMock = RequestMock()
    .onRequestTo('http://localhost:3016/horse')
    .respond([{
        id: "6e6c9cc4-1092-4cd4-8cb0-e4ab46446d67",
        name: "new horse",
        profile: {
            favouriteFood: "Hot Chips",
            physical: {
                height: 200,
                weight: 450
            }
        }
    }, {
        id: "e4405a0d-8674-40bb-8ecb-541017f942cc",
        name: "Artax",
        profile:
            { favouriteFood: "Carrots", physical: { height: 198.99, weight: 4001 } }
    }, {
        id: "df4a5567-85eb-4fb5-8322-532b5e621f3e",
        name: "Potoooooooo2",
        profile: {
            favouriteFood: "Potatoes", physical: { height: 205.44, weight: 423.44 }
        }
    }, {
        id: "f85d9de9-0116-4758-a610-8fe11ff7d746",
        name: "Shorty45146",
        profile: {
            favouriteFood: "Kebabs",
            physical: { height: 180.66, weight: 500.23 }
        }
    }, {
        id: "a95156b4-1ab2-45c6-94ae-fa270be85989",
        name: "dsada",
        profile: {
            favouriteFood: null,
            physical: { height: null, weight: null }
        }
    }, {
        id: "cf1ff837-f7a7-4609-9856-f3bd2428a1d8",
        name: "radasd",
        profile: {
            favouriteFood: null,
            physical: { height: null, weight: null }
        }
    }, {
        id: "a24e826f-16f7-42b0-ac50-9df134968a34",
        name: "fefe",
        profile: {
            favouriteFood: null,
            physical: { height: null, weight: null }
        }
    }, {
        id: "e77744fd-aac3-4df5-b8b6-cb82451fb91c",
        name: "123214",
        profile: {
            favouriteFood: null,
            physical: { height: null, weight: null }
        }
    }, {
        id: "bd079a0c-b4f8-44e6-88de-30f52794b38c",
        name: "test132",
        profile: {
            favouriteFood: null,
            physical: { height: null, weight: null }
        }
    }, {
        id: "2885ae7e-4721-4b1c-91ce-a7395145c4a8",
        name: "test1244",
        profile: {
            favouriteFood: "2131",
            physical: { height: 3231, weight: 32131 }
        }
    }, {
        id: "16a701da-323d-45cb-b5c2-cebbf4771765",
        name: "11number",
        profile: {
            favouriteFood: "123",
            physical: { height: 123, weight: 123 }
        }
    }, {
        id: "9b7fff0f-5d0d-456d-9197-ed968d8574b6",
        name: "jjlw",
        profile: {
            favouriteFood: "jjl food",
            physical: { height: 123, weight: null }
        }
    }, {
        id: "6cc9ce4c-19cb-4d3a-9364-6456574f5eff",
        name: "Jiajie Liu2",
        profile: {
            favouriteFood: "dasda",
            physical: { height: 12, weight: null }
        }
    }, {
        id: "6ec451b3-9f46-45ec-8c7b-9648c9c00b6c",
        name: "Jiajie Liu1",
        profile: {
            favouriteFood: "te",
            physical: { height: 123, weight: null }
        }
    }, {
        id: "6a845b54-96da-45a1-bc31-8a7df7adfca2",
        name: "dsad",
        profile: { favouriteFood: "dsad", physical: { height: 123, weight: 213 } }
    }], 200, { 'Access-Control-Allow-Origin': '*' });

fixture`Horses test with multiple data`.page // declare the fixture
    `http://localhost:3000/`
    .requestHooks(multipleDataMock);

// Test
test('Should to only see 10 records per page and able to switch page', async t => {
    const nextButton = Selector('button.MuiButtonBase-root').withAttribute('aria-label', 'Next page');
    const count = Selector('td.MuiTableCell-body').withAttribute('data-testid', 'horse-name-cell').count;
    await t.expect(count).eql(10);
    await t.click(nextButton);
    await t.expect(count).eql(5);
});