### Read me ###

## Page information

* This is project will display a table with a list of horses.
* The table will contains "name", "favourite food" & "actions".
* There will be 10 records per page, and user can switch to next page by click 'next page button'.
* User can also choose how many records they want to show by click the 'row per page' dropdown

* In the "actions" column, user can choose to view & edit a particular horse

* When user view the details for one horse (click view button), a dialog will be displayed with title 'horse details' and some details of that horse. User can click 'ok' button to dismiss the dialog.

* When user edit a horse, a dialog will be displayed which will allow user to edit that horse's details, the old details will be preload in that dialog.
* The dialog will contains two button 'save' & 'cancel'
* The name field can not be null, if user didn't provide name, when they click the 'save' button, the dialog won't be dismissed, and the 'name input field' will be marked as 'red'

* There are two button in the page, just below the table:
'Add' button, which will trigger a dialog which will allow user to add a new horse to the
'Compare' button, by default, it will be disabled, it will be enabled when user selected two horses, and a dialog with two selected horses details will be displayed after user click this button

* Checkbox - each table row contains a checkbox, user can used it to select a horse which they want to compare.
* If user selected two horses, the rest checkboxs will be disabled.

## What I have done
1. UI Components 
2. testCafe integration tests - will test add/update horses, compare horses & view horses & pagination
3. Jest tests for testing Slices & thunk
4. A simple snapshot test

## TODO
1. Global error handling - error already stored in the state. 
2. Test more UI cases - i.e. the checkbox will remain selected when user close the compare dialog
3. I didn't call getHorseById api, as I found it returns the same data as getHorseList returned. Ideally, when user selected to view a horse, we should make a call to get that horse's details and display it in the ui. (happy to discuss more details about this).
4. Empty state

## Suggestion
1. Make update/Add api return the same response format, so that make UI more clean and easy to map

## Available Scripts

In the project directory, you can run:

### `npm run start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm run test`

Launches the test runner in the interactive watch mode.<br />

### `npm run integration-test`

This will launch testCafe test which will go through different use cases.
Open (https://devexpress.github.io/testcafe/) for more details
