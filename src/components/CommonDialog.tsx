import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

interface CommonDialogProps {
    open: boolean;
    title: string;
    dialogBody: React.ReactNode;
    onClose: () => void;
    leftButtonAction?: () => void;
    rightButtonAction?: () => void;
    leftButtonText?: string;
    rightButtonText?: string;
    disableRightButton?: boolean;
    disableLeftButton?: boolean;
}

// Shared dialog component
export default function CommonDialog(props: CommonDialogProps) {
    const { open, onClose, leftButtonAction, rightButtonAction, title, dialogBody, leftButtonText, rightButtonText, disableRightButton, disableLeftButton } = props;

    if (!open) {
        return null;
    }

    return (
        <div>
            <Dialog
                open={open}
                onClose={onClose}
                aria-labelledby="common-dialog-title"
                aria-describedby="common-dialog-description"
            >
                <DialogTitle id="common-dialog-title">{title}</DialogTitle>
                <DialogContent>
                    {dialogBody}
                </DialogContent>
                <DialogActions>
                    {leftButtonText && <Button onClick={leftButtonAction} color="primary" disabled={disableLeftButton}>
                        {leftButtonText}
                    </Button>}
                    {rightButtonText && <Button onClick={rightButtonAction} color="secondary" disabled={disableRightButton} autoFocus>
                        {rightButtonText}
                    </Button>}
                </DialogActions>
            </Dialog>
        </div>
    );
}
