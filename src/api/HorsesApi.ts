import { Horse } from "../models/api/horses/horses";
import { convertFromApiModel } from "../models/helpers/horsesModelConverter";
import { ApiServiceClient } from "../utils/ApiServiceClient";

const client: ApiServiceClient = new ApiServiceClient();

export async function GetHorses() {
    var result = await client.get<Horse[]>(
        "horse"
    );
    if (result && result.length > 0) {
        return result.map(x => {
            return convertFromApiModel(x);
        });
    }
}

export async function SaveHorse(isAdd: boolean, horse: Horse) {
    var result = await client.put(
        isAdd ? `horse` : `horse/${horse.id}`,
        horse
    );

    if (isAdd) {
        return {
            ...convertFromApiModel(horse),
            id: result
        }
    }

    return convertFromApiModel(result as Horse);
}