import React from 'react';
import { createMuiTheme, ThemeProvider } from "@material-ui/core/styles";
import { Horses } from './features/horses/components/Horses';

//Initialize material ui
const theme = createMuiTheme();

function App() {
  return (
    <div className="App">
      <ThemeProvider theme={theme}>
        <Horses />
      </ThemeProvider>
    </div>
  );
}

export default App;
