var Core = {
    service: {
        serviceBase: '',
        token: '',
        initializeCore: function (serviceBase) {
            this.serviceBase = serviceBase;
        },
        setToken: function (newToken) {
            if (!newToken) {
                return;
            }
            this.token = newToken;
        }
    }
}
module.exports = Core;