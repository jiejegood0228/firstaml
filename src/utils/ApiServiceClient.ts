import axios from "axios";
import * as core from "../core";

export class ApiServiceClient {

    public async get<T>(url: string): Promise<T> {
        var result = await axios({
            method: "GET",
            url: `${core.service.serviceBase}/${url}`,
        });
        return result.data as T;
    }

    public async put<T>(url: string, data?: any): Promise<T> {
        var result = await axios({
            method: "PUT",
            url: `${core.service.serviceBase}/${url}`,
            data: data,
        });
        return result.data as T;
    }

}
