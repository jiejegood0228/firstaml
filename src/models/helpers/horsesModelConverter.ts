import { Horse } from "../api/horses/horses";
import { HorseModel } from "../ui/horses/horses";

export const convertFromApiModel = (horse: Horse): HorseModel => {
    debugger;
    if (horse) {
        return {
            id: horse.id,
            name: horse.name,
            favouriteFood: horse.profile?.favouriteFood,
            height: horse.profile?.physical?.height,
            weight: horse.profile?.physical?.weight
        }
    }
    return null;
}

export const convertToApiModel = (horse: HorseModel) => {
    if (horse) {
        return {
            id: horse.id,
            name: horse.name,
            profile: {
                favouriteFood: horse.favouriteFood,
                physical: {
                    height: horse.height ? Number(horse.height) : null,
                    weight: horse.weight ? Number(horse.weight) : null
                }
            }
        }
    }
    return null;
}

// http://localhost:3016/horse