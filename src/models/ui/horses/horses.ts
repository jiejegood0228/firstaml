export interface HorseModel {
    id?: string;
    name: string;
    favouriteFood?: string;
    height?: number;
    weight?: number;
}

export interface Column {
    id: string;
    label: string;
    minWidth?: number;
}

