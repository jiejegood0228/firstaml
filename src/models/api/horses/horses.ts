import { HorseModel } from "../../ui/horses/horses";

export interface Horse {
    id: string;
    name: string;
    profile?: HorseProfile;
}

interface HorseProfile {
    favouriteFood?: string;
    physical?: Physical;
}

interface Physical {
    height?: number;
    weight?: number;
}

export interface SaveHorseRequest {
    isAdd: boolean;
    horseModel: HorseModel;
}