import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import horsesReducer from '../features/horses/HorsesSlice';

export const store = configureStore({
  reducer: {
    horses: horsesReducer
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
