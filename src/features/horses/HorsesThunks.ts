import { createAsyncThunk } from "@reduxjs/toolkit";
import { GetHorses, SaveHorse } from "../../api/HorsesApi";
import { SaveHorseRequest } from "../../models/api/horses/horses";
import { convertToApiModel } from "../../models/helpers/horsesModelConverter";

// Fetch horses list
export const fetchHorsesList = createAsyncThunk(
    "horses/fetchHorsesList",
    async () => {
        return await GetHorses();
    }
);

// Add or update horse
export const addOrUpdateHorse = createAsyncThunk(
    "horses/saveHorse",
    async (request: SaveHorseRequest) => {
        const horse = convertToApiModel(request.horseModel);
        return await SaveHorse(request.isAdd, horse)
    }
);