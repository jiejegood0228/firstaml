import { createSlice, createEntityAdapter } from '@reduxjs/toolkit';
import { RootState } from '../../app/store';
import { HorseModel } from '../../models/ui/horses/horses';
import { fetchHorsesList, addOrUpdateHorse } from './HorsesThunks';

export const horsesAdapter = createEntityAdapter<HorseModel>({
    selectId: (horse) => horse.id,
});

export const horsesSlice = createSlice({
    name: 'horses',
    initialState: horsesAdapter.getInitialState({
        loading: false,
        error: '',
        updating: false // used for saving the changes, ideally it will affect the UI, but I didn't apply it this time
    }),
    reducers: {
    },
    extraReducers: {
        [fetchHorsesList.pending.toString()]: (state, action) => {
            state.loading = true;
        },
        [fetchHorsesList.fulfilled.toString()]: (state, action) => {
            state.loading = false;
            horsesAdapter.setAll(state, action.payload)
        },
        [fetchHorsesList.rejected.toString()]: (state, action) => {
            state.loading = false
            state.error = action.error
        },
        [addOrUpdateHorse.pending.toString()]: (state) => {
            state.updating = true;
        },
        [addOrUpdateHorse.fulfilled.toString()]: (state, action) => {
            state.updating = false;
            horsesAdapter.upsertOne(state, action.payload)
        },
        [addOrUpdateHorse.rejected.toString()]: (state, action) => {
            state.updating = false
            state.error = action.error
        }
    },
});

export const loadingHorses = (state: RootState) => state.horses.entities.loading;
export const { selectAll } = horsesAdapter.getSelectors((state: RootState) => state.horses);

export default horsesSlice.reducer;
