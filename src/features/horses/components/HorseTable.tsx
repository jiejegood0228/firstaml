import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import IconButton from '@material-ui/core/IconButton';
import EditIcon from '@material-ui/icons/Edit';
import SearchIcon from '@material-ui/icons/Search';
import { Checkbox } from '@material-ui/core';
import { Column, HorseModel } from '../../../models/ui/horses/horses';

const columns: Column[] = [
    { id: 'check', label: '', minWidth: 50 },
    { id: 'name', label: 'Name', minWidth: 170 },
    { id: 'favouriteFood', label: 'Favourite Food', minWidth: 200 },
    {
        id: 'actions',
        label: 'actions',
        minWidth: 170,
    }
];

const useStyles = makeStyles({
    root: {
        width: '100%',
    },
    container: {
        maxHeight: 440,
    },
});

interface HorseTableProps {
    horses: HorseModel[];
    onUpdateHorse: (horse: HorseModel) => void;
    selectedHorses: HorseModel[]
    handleSelectHorse: (horse: HorseModel, event: React.ChangeEvent<HTMLInputElement>) => void;
    onViewDetails: (horse: HorseModel) => void;
}

const maxPerPage = 10;

// Display a table which will list all the horses data
// Ideally, we can make it as a shared component.
// Will only show 10 records per page
export default function HorseTable(props: HorseTableProps) {
    const classes = useStyles();
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(maxPerPage);
    const { horses, onUpdateHorse, selectedHorses, handleSelectHorse, onViewDetails } = props;

    const handleChangePage = React.useCallback((event: unknown, newPage: number) => {
        setPage(newPage);
    }, []);

    const handleChangeRowsPerPage = React.useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
        setRowsPerPage(+event.target.value);
        setPage(0);
    }, []);

    const filteredHorses = React.useMemo(() => {
        return horses.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage);
    }, [horses, page, rowsPerPage]);

    if (!horses || !horses.length) {
        return null; // todo empty statee
    }

    return (
        <>
            <Paper className={classes.root}>
                <TableContainer className={classes.container}>
                    <Table stickyHeader>
                        <TableHead>
                            <TableRow>
                                {columns.map((column) => (
                                    <TableCell
                                        key={column.id}
                                        style={{ minWidth: column.minWidth }}
                                    >
                                        {column.label}
                                    </TableCell>
                                ))}
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {filteredHorses.map((horse, index) => {
                                const isRowSelected = !!selectedHorses.find(x => x.id === horse.id);
                                return (
                                    <TableRow hover key={index}>
                                        <TableCell padding="checkbox">
                                            <Checkbox
                                                checked={isRowSelected}
                                                onChange={(event) => handleSelectHorse(horse, event)}
                                                disabled={selectedHorses.length === 2 && !isRowSelected}
                                                id={`check-${horse.id}`}
                                            />
                                        </TableCell>
                                        <TableCell data-testid="horse-name-cell">
                                            {horse.name}
                                        </TableCell>
                                        <TableCell  >
                                            {horse.favouriteFood}
                                        </TableCell>
                                        <TableCell >
                                            <IconButton id={`view-${horse.id}`} aria-label={`view ${horse.name}`} onClick={() => onViewDetails(horse)}>
                                                <SearchIcon />
                                            </IconButton>
                                            <IconButton d={`edit-${horse.id}`} aria-label={`edit ${horse.name}`} onClick={() => onUpdateHorse(horse)}>
                                                <EditIcon />
                                            </IconButton>
                                        </TableCell>
                                    </TableRow>
                                );
                            })}
                        </TableBody>
                    </Table>
                </TableContainer>
                <TablePagination
                    rowsPerPageOptions={[10, 25, 100]} // TODO make it constant
                    component="div"
                    count={horses.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onChangePage={handleChangePage}
                    onChangeRowsPerPage={handleChangeRowsPerPage}
                />
            </Paper>
        </>
    );
}
