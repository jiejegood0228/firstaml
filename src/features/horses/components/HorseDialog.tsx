import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import CommonDialog from '../../../components/CommonDialog';
import { HorseModel } from '../../../models/ui/horses/horses';

interface HorseCompareDialogProps {
  open: boolean;
  onClose: () => void;
  horses: HorseModel[];
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex', justifyContent: 'space-between', width: 500,
      '& .MuiTextField-root': {
        margin: theme.spacing(1),
        width: '25ch',
      },
    },
    container: {
      border: '1px solid', width: 220, padding: 25
    }
  }),
);

// A dialog for compare two horse or display horse details,
export default function HorseDialog(props: HorseCompareDialogProps) {
  const { open, onClose, horses } = props;
  const classes = useStyles();

  const viewOnly = React.useMemo(() => {
    return horses.length === 1;
  }, [horses]);

  if (!open || !horses || !horses.length) {
    return null;
  }

  return <CommonDialog
    open
    leftButtonText="Ok"
    leftButtonAction={onClose}
    onClose={onClose}
    title={viewOnly ? 'horse details' : "Compare horses"}
    dialogBody={< div className={classes.root} >
      {
        horses.map(h => {
          return <div key={h.id} className={classes.container}>
            <div data-testid="compare-name">
              name: {h.name}
            </div>
            <div>
              favouriteFood: {h.favouriteFood}
            </div>
            <div>
              height: {h.height}
            </div>
            <div>
              weight: {h.weight}
            </div>
          </div>
        })
      }
    </div >}
  />
}
