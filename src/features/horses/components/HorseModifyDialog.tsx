import React from 'react';
import { TextField } from '@material-ui/core';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import CommonDialog from '../../../components/CommonDialog';
import { HorseModel } from '../../../models/ui/horses/horses';
import { useDispatch } from 'react-redux';
import { addOrUpdateHorse } from '../HorsesThunks';

interface HorseModifyDialogProps {
  open: boolean;
  onClose: () => void;
  horseToUpdate?: HorseModel;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      '& .MuiTextField-root': {
        margin: theme.spacing(1),
        width: '25ch',
      },
    },
  }),
);

// This component is used to edit/add horse
export default function HorseModifyDialog(props: HorseModifyDialogProps) {
  const { open, onClose, horseToUpdate } = props;
  const dispatch = useDispatch();
  const classes = useStyles();
  const [newHorse, setNewHorse] = React.useState(null); // for edit horse or new horse
  const [showError, setShowError] = React.useState(false); // only show error when user click the save button
  const [touched, setTouched] = React.useState(false); //check if user has changed something

  React.useEffect(() => {
    setNewHorse(horseToUpdate)
  }, [horseToUpdate]);

  // check is add new horse
  const isAdd = React.useMemo(() => !horseToUpdate, [horseToUpdate]);

  const handleClose = React.useCallback(() => {
    setNewHorse(null);
    setShowError(false);
    setTouched(false);
    onClose();
  }, [onClose]);

  // Save the changes
  const onSave = React.useCallback(() => {
    if (!newHorse?.name) {
      setShowError(true);
      return;
    }
    dispatch(addOrUpdateHorse({
      isAdd,
      horseModel: newHorse
    }));
    handleClose();
  }, [dispatch, handleClose, isAdd, newHorse])

  // Used when user type text for textfield, i.e: name/weight
  const handleChange = React.useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
    event.preventDefault();
    const field = event.target.name;
    setNewHorse({
      ...newHorse,
      [field]: event.target.value
    });
    setTouched(true);
  }, [newHorse])

  const title = React.useMemo(() => {
    return isAdd ? 'Add' : 'Edit';
  }, [isAdd]);

  if (!open) {
    return null;
  }

  return <CommonDialog
    open
    leftButtonText={'Cancel'}
    leftButtonAction={handleClose}
    onClose={handleClose}
    rightButtonText={'Save'}
    rightButtonAction={onSave}
    disableRightButton={!touched}
    title={title}
    dialogBody={<div className={classes.root}>
      <TextField required id="name-input" name="name" label="Name" defaultValue={newHorse?.name} error={showError} onChange={handleChange} />
      <TextField id="favouriteFood-input" name="favouriteFood" label="Favourite food" defaultValue={newHorse?.favouriteFood} onChange={handleChange} />
      <TextField id="height-input" name="height" label="height" type="number" defaultValue={newHorse?.height} onChange={handleChange} />
      <TextField id="weight-input" name="weight" label="weight" type="number" defaultValue={newHorse?.weight} onChange={handleChange} />
    </div>}
  />
}
