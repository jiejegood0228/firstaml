import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import { useDispatch, useSelector } from "react-redux";
import { selectAll, loadingHorses } from '../HorsesSlice';
import HorseTable from './HorseTable';
import HorseModifyDialog from './HorseModifyDialog';
import { Button, ButtonGroup } from '@material-ui/core';
import HorseDialog from './HorseDialog';
import { HorseModel } from '../../../models/ui/horses/horses';
import { fetchHorsesList } from '../HorsesThunks';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      padding: 45
    },
    buttonGroup: {
      paddingTop: 25, float: 'right',
      '&> button': {
        margin: 25
      }
    },

  }),
);

export function Horses() {
  const dispatch = useDispatch();
  const classes = useStyles();
  const horses = useSelector(selectAll);
  const loading = useSelector(loadingHorses);
  const [openModifyDialog, setOpenModifyDialog] = React.useState(false);
  const [openHorseDialog, setOpenHorseDialog] = React.useState(false);
  const [selectedHorses, setSelectedHorses] = React.useState([] as HorseModel[]);
  const [selectedHorse, setSelectedHorse] = React.useState(null);
  const [isViewOnly, setIsViewOnly] = React.useState(false);

  React.useEffect(() => {
    dispatch(fetchHorsesList());
  }, [dispatch]);

  // Used when user clicked the edit button for a horse recird
  const handleUpdateHorse = React.useCallback((horse: HorseModel) => {
    horse && setSelectedHorse(horse);
    setOpenModifyDialog(true);
  }, []);

  // Used when close/open horses dialog
  const handleHorsesDialog = React.useCallback((viewOnly: boolean) => {
    setOpenHorseDialog(!openHorseDialog);
    setIsViewOnly(viewOnly);
  }, [openHorseDialog]);

  // Used when user close the view horse dialog
  // It should reset the selected horse 
  // It doesn't reset selected horses for compare, as we want to keep them selected
  const handleCloseHorseDialog = React.useCallback(() => {
    handleHorsesDialog(false);
    setSelectedHorse(null);
  }, [handleHorsesDialog]);

  // Used when user clicked the view horse details button
  const handleViewHorse = React.useCallback((horse: HorseModel) => {
    horse && setSelectedHorse(horse);
    handleHorsesDialog(true);
  }, [handleHorsesDialog]);

  // when user close the HorseModifyDialog
  const handleCloseModifyDialog = React.useCallback(() => {
    setSelectedHorse(null);
    setOpenModifyDialog(false);
    setIsViewOnly(false);
  }, []);

  // Used when user select two horses for comparison
  const handleSelectHorse = React.useCallback((horse: HorseModel, event: React.ChangeEvent<HTMLInputElement>) => {
    var checked = event.target.checked;
    if (checked) {
      setSelectedHorses([...selectedHorses].concat(horse));
    } else {
      setSelectedHorses([...selectedHorses.filter(x => x.id !== horse.id)]);
    }
  }, [selectedHorses]);

  return (
    <div className={classes.root}>
      <CssBaseline />
      <HorseModifyDialog open={openModifyDialog} horseToUpdate={selectedHorse} onClose={handleCloseModifyDialog} />
      <HorseDialog open={openHorseDialog} horses={isViewOnly ? [selectedHorse] : selectedHorses} onClose={handleCloseHorseDialog} />
      {loading && 'loading'}
      <HorseTable horses={horses} onUpdateHorse={handleUpdateHorse} handleSelectHorse={handleSelectHorse} selectedHorses={selectedHorses} onViewDetails={handleViewHorse} />
      <ButtonGroup color="primary" className={classes.buttonGroup}>
        <Button variant="contained" disabled={selectedHorses.length < 2} color="primary" onClick={() => handleHorsesDialog(false)}>
          Compare
        </Button>
        <Button variant="contained" color="secondary" onClick={() => handleUpdateHorse(null)} >
          Add
      </Button>
      </ButtonGroup>
    </div>
  );
}
