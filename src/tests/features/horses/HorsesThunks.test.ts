import { fetchHorsesList, addOrUpdateHorse } from "../../../features/horses/HorsesThunks";
import * as Api from '../../../api/HorsesApi';
import { HorseModel } from "../../../models/ui/horses/horses";

describe("horsesThunks", () => {
    describe('fetchHorsesList', () => {
        it('auto trigger pending and reject action', async () => {
            // Arrange
            const dispatch = jest.fn();

            // Act
            const thunkPromise = fetchHorsesList()(dispatch, () => { }, undefined);

            await thunkPromise;

            // Assert
            expect(dispatch).toBeCalledTimes(2);

            expect(dispatch.mock.calls[0][0].type).toEqual(
                fetchHorsesList.pending.toString()
            );

            expect(dispatch.mock.calls[1][0].type).toEqual(
                fetchHorsesList.rejected.toString()
            );
        });

        it('auto fulfilled action', async () => {
            // Arrange
            const data: HorseModel = {
                id: "5bb04e04-5698-4237-a1c7-d2eb285c5d51",
                name: "Thunderdash",
                favouriteFood: "Hot Chips",
                height: 200,
                weight: 450
            };

            const dispatch = jest.fn();

            jest.spyOn(Api, 'GetHorses').mockResolvedValueOnce([data]);

            // Act
            const thunkPromise = fetchHorsesList()(dispatch, () => { }, undefined);

            await thunkPromise;

            // Assert
            expect(dispatch).toBeCalledTimes(2);

            expect(dispatch.mock.calls[0][0].type).toEqual(
                fetchHorsesList.pending.toString()
            );

            // Expect fulfilled call
            expect(dispatch.mock.calls[1][0].type).toEqual(
                fetchHorsesList.fulfilled.toString()
            );

            expect(dispatch.mock.calls[1][0].payload).toEqual(
                [data]
            );
        });
    });

    describe('addOrUpdateHorse', () => {
        it('auto trigger pending and reject action', async () => {
            // Arrange
            const dispatch = jest.fn();
            const data: HorseModel = {
                id: "5bb04e04-5698-4237-a1c7-d2eb285c5d51",
                name: "Thunderdash",
                favouriteFood: "Hot Chips",
                height: 200,
                weight: 450
            };

            // Act
            const thunkPromise = addOrUpdateHorse({
                isAdd: false,
                horseModel: data
            })(dispatch, () => { }, undefined);

            await thunkPromise;

            // Assert
            expect(dispatch).toBeCalledTimes(2);

            expect(dispatch.mock.calls[0][0].type).toEqual(
                addOrUpdateHorse.pending.toString()
            );

            expect(dispatch.mock.calls[1][0].type).toEqual(
                addOrUpdateHorse.rejected.toString()
            );
        });

        it('should call fulfilled with updated horse when update', async () => {
            // Arrange
            const data: HorseModel = {
                id: "5bb04e04-5698-4237-a1c7-d2eb285c5d51",
                name: "Thunderdash",
                favouriteFood: "Hot Chips",
                height: 200,
                weight: 450
            };

            const updatedData: HorseModel = {
                id: "5bb04e04-5698-4237-a1c7-d2eb285c5d51",
                name: "Updated",
                favouriteFood: "Updated Chips",
                height: 200,
                weight: 450
            };

            const dispatch = jest.fn();

            jest.spyOn(Api, 'SaveHorse').mockResolvedValueOnce(updatedData);

            // Act
            const thunkPromise = addOrUpdateHorse({
                isAdd: false,
                horseModel: data
            })(dispatch, () => { }, undefined);

            await thunkPromise;

            // Assert
            expect(dispatch).toBeCalledTimes(2);

            expect(dispatch.mock.calls[0][0].type).toEqual(
                addOrUpdateHorse.pending.toString()
            );

            // Expect fulfilled call
            expect(dispatch.mock.calls[1][0].type).toEqual(
                addOrUpdateHorse.fulfilled.toString()
            );

            expect(dispatch.mock.calls[1][0].payload).toEqual(
                updatedData
            );
        });

        it('should call fulfilled with new horse was added', async () => {
            // Arrange
            const data: HorseModel = {
                name: "Thunderdash",
                favouriteFood: "Hot Chips",
                height: 200,
                weight: 450
            };

            const newId = "5bb04e04-5698-4237-a1c7-d2eb285c5d51";

            const updatedData: HorseModel = {
                id: newId,
                name: "Updated",
                favouriteFood: "Updated Chips",
                height: 200,
                weight: 450
            };

            const dispatch = jest.fn();

            jest.spyOn(Api, 'SaveHorse').mockResolvedValueOnce(updatedData);

            // Act
            const thunkPromise = addOrUpdateHorse({
                isAdd: true,
                horseModel: data
            })(dispatch, () => { }, undefined);

            await thunkPromise;

            // Assert
            expect(dispatch).toBeCalledTimes(2);

            expect(dispatch.mock.calls[0][0].type).toEqual(
                addOrUpdateHorse.pending.toString()
            );

            // Expect fulfilled call
            expect(dispatch.mock.calls[1][0].type).toEqual(
                addOrUpdateHorse.fulfilled.toString()
            );

            expect(dispatch.mock.calls[1][0].payload).toEqual(
                updatedData
            );
        });
    });
});