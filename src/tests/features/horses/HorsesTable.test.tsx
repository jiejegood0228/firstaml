import React from 'react';
import * as snapshotRenderer from 'react-test-renderer';
import HorseTable from '../../../features/horses/components/HorseTable';

test('renders learn react link', () => {
    const component = (
        <HorseTable horses={[{
            name: 'test',
            favouriteFood: 'testfood',
            weight: 100,
            height: 100
        }]} onUpdateHorse={jest.fn()} handleSelectHorse={jest.fn()} selectedHorses={[]} onViewDetails={jest.fn()} />
    );

    const tree = snapshotRenderer.create(component).toJSON();
    expect(tree).toMatchSnapshot()
});
