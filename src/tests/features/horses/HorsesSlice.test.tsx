import { RootState, store } from "../../../app/store";
import horsesReducer, { horsesAdapter, selectAll } from '../../../features/horses/HorsesSlice';
import { fetchHorsesList, addOrUpdateHorse } from "../../../features/horses/HorsesThunks";
import { HorseModel } from "../../../models/ui/horses/horses";

const appState = store.getState();

describe("horsesSlice", () => {
    describe("fetchHorseList", () => {
        it("should set loading state to true when fetchHorseList", () => {
            // Arrange
            // Act
            const nextState = horsesReducer(
                horsesAdapter.getInitialState({
                    loading: false,
                    error: '',
                    updating: false
                }),
                fetchHorsesList.pending
            );

            // Assert
            const rootState: RootState = { ...appState, horses: nextState };
            expect(rootState.horses.loading).toBeTruthy();
        });

        it("should set horses when fetch success", () => {
            // Arrange
            const data: HorseModel = {
                id: "5bb04e04-5698-4237-a1c7-d2eb285c5d51",
                name: "Thunderdash",
                favouriteFood: "Hot Chips",
                height: 200,
                weight: 450
            };

            // Act
            const nextState = horsesReducer(
                horsesAdapter.getInitialState({
                    loading: false,
                    error: '',
                    updating: false
                }),
                fetchHorsesList.fulfilled([data], "")
            );

            // Assert
            const rootState: RootState = { ...appState, horses: nextState };
            const allHorses = selectAll(rootState);
            expect(allHorses.length).toEqual(1);
            expect(allHorses[0].id).toEqual(data.id);
            expect(allHorses[0].name).toEqual(data.name);
            expect(allHorses[0].favouriteFood).toEqual(data.favouriteFood);
            expect(allHorses[0].height).toEqual(data.height);
            expect(allHorses[0].weight).toEqual(data.weight);
            expect(rootState.horses.loading).toBeFalsy();
        });

        it("should set loading state to false when fetchHorseList failed", () => {
            // Arrange
            // Act
            const nextState = horsesReducer(
                horsesAdapter.getInitialState({
                    loading: false,
                    error: '',
                    updating: false
                }),
                fetchHorsesList.rejected
            );

            // Assert
            const rootState: RootState = { ...appState, horses: nextState };
            expect(rootState.horses.loading).toBeFalsy();
        });
    });

    describe("addOrUpdateHorse", () => {
        it("should set updating state to true when save Horse", () => {
            // Arrange

            // Act
            const nextState = horsesReducer(
                horsesAdapter.getInitialState({
                    loading: false,
                    error: '',
                    updating: false
                }),
                addOrUpdateHorse.pending
            );

            // Assert
            const rootState: RootState = { ...appState, horses: nextState };
            expect(rootState.horses.updating).toBeTruthy();
        });

        it("should set horses when save a new Horse success", () => {
            // Arrange
            const data: HorseModel = {
                id: "5bb04e04-5698-4237-a1c7-d2eb285c5d51",
                name: "Thunderdash2",
                favouriteFood: "Hot Chips2",
                height: 200,
                weight: 450
            };

            // Act
            const nextState = horsesReducer(
                horsesAdapter.getInitialState({
                    loading: false,
                    error: '',
                    updating: false
                }),
                addOrUpdateHorse.fulfilled(data, "", {
                    isAdd: true,
                    horseModel: data
                })
            );

            // Assert
            const rootState: RootState = { ...appState, horses: nextState };
            const allHorses = selectAll(rootState);
            expect(allHorses.length).toEqual(1);
            expect(allHorses[0].id).toEqual(data.id);
            expect(allHorses[0].name).toEqual(data.name);
            expect(allHorses[0].favouriteFood).toEqual(data.favouriteFood);
            expect(allHorses[0].height).toEqual(data.height);
            expect(allHorses[0].weight).toEqual(data.weight);
            expect(rootState.horses.updating).toBeFalsy();
        });

        it("should update existing horse", () => {
            // Arrange
            const data: HorseModel = {
                id: "5bb04e04-5698-4237-a1c7-d2eb285c5d51",
                name: "Thunderdash2",
                favouriteFood: "Hot Chips2",
                height: 200,
                weight: 450
            };

            const updateData: HorseModel = {
                id: "5bb04e04-5698-4237-a1c7-d2eb285c5d51",
                name: "update",
                favouriteFood: "update chip",
                height: 200,
                weight: 450
            };

            // Act
            const state = horsesAdapter.getInitialState({
                loading: false,
                error: '',
                updating: false
            });

            //Add original data
            horsesAdapter.upsertOne(state, data);

            const nextState = horsesReducer(
                state,
                addOrUpdateHorse.fulfilled(updateData, "", {
                    isAdd: true,
                    horseModel: updateData
                })
            );

            // Assert
            const rootState: RootState = { ...appState, horses: nextState };
            const allHorses = selectAll(rootState);
            expect(allHorses.length).toEqual(1);
            expect(allHorses[0].id).toEqual(data.id);
            expect(allHorses[0].name).toEqual(updateData.name);
            expect(allHorses[0].favouriteFood).toEqual(updateData.favouriteFood);
            expect(allHorses[0].height).toEqual(data.height);
            expect(allHorses[0].weight).toEqual(data.weight);
            expect(rootState.horses.updating).toBeFalsy();
        });

        it("should set update state to false when saveHorse failed", () => {
            // Arrange
            // Act
            const nextState = horsesReducer(
                horsesAdapter.getInitialState({
                    loading: false,
                    error: '',
                    updating: false
                }),
                fetchHorsesList.rejected
            );

            // Assert
            const rootState: RootState = { ...appState, horses: nextState };
            expect(rootState.horses.updating).toBeFalsy();
        });
    });

});